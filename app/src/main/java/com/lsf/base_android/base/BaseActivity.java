package com.lsf.base_android.base;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;

import com.lsf.base_android.utils.ActivityUtil;
import com.lsf.base_android.utils.LogUtil;
import com.lsf.base_android.utils.MyApplication;
import com.lsf.base_android.utils.OkHttpUtil;
import com.lsf.base_android.utils.ToastUtil;

public abstract class BaseActivity extends FragmentActivity implements
        OnClickListener,
        OkHttpUtil.OkHttpCallback {
    public BaseActivity context;
    public LogUtil mLog;
    public ToastUtil mToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 清空系统背景图片
        getWindow().setBackgroundDrawable(null);
        context = this;
        mLog = MyApplication.mLog;
        mToast = MyApplication.mToast;
        ActivityUtil.getInstance().pushOneActivity(this);


    }

    public OkHttpUtil OkHttp() {
        return OkHttpUtil.getInstance();
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub

        super.onStart();
    }

    /**
     * 功能屏蔽
     */
    public void ViewGone() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        ActivityUtil.getInstance().popOneActivity(this);
        OkHttp().mOkHttpClient.cancel(context.getLocalClassName());
        super.onDestroy();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    /**
     * 设置布局文件
     */
    public abstract void setView();

    /**
     * 设置控件的监听
     */
    public abstract void setListener();

    /**
     * 加载数据
     */
    public abstract void initData();


    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }


}
