package com.lsf.base_android.activity;

import android.os.Bundle;

import com.lsf.base_android.R;
import com.lsf.base_android.base.BaseActivity;


public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void setView() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void initData() {

    }


    @Override
    public void onSuccess(String result, int code) {

    }

    @Override
    public void onStart(int code) {

    }

    @Override
    public void onFailure(int code) {

    }
}
