package com.lsf.base_android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lsf.base_android.R;
import com.lsf.base_android.base.BaseActivity;
import com.lsf.base_android.setting.HttpConstants;
import com.lsf.base_android.utils.DoubleClickListener;
import com.lsf.base_android.utils.OkHttpUtil;

import java.util.HashMap;


public class MainActivity2 extends BaseActivity {

    @ViewInject(R.id.mTv)
    public TextView mTv;

    @ViewInject(R.id.mImg)
    public ImageView mImg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewUtils.inject(this);
        setView();
        setListener();
        initData();
    }

    /**
     * 初始化view
     */
    @Override
    public void setView() {
        Glide.with(this).load("http://img2.niushe.com/upload/201304/19/14-22-45-63-26144.jpg").into(mImg);
    }

    /**
     * 设置监听
     */
    @Override
    public void setListener() {
        mTv.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onNoDoubleClick(View v) {

            }
        });
    }

    @Override
    public void onClick(View arg0) {
        super.onClick(arg0);
    }

    /**
     * 初始化数据
     */
    @Override
    public void initData() {
        //资讯
        getNews();
    }

    /**
     * 资讯
     */
    private void getNews() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("a", "s");
        OkHttp().getOkHttp("news", HttpConstants.SEARCH_NEWS, map, this
        ).setOnStartListener(new OkHttpUtil.OkStartListener() {
            @Override
            public void onStart(int code) {

            }
        }).setOnSuccessListener(new OkHttpUtil.OkSuccessListener() {
            @Override
            public void onSuccess(String result, int code) {
                mTv.setText(result);
                mLog.i(result.toString() + "");
                mToast.showToast(result + "");
            }
        }).setOnFailureListener(new OkHttpUtil.OkFailureListener() {
            @Override
            public void onFailure(int code) {

            }
        });
    }

    /**
     * 请求成功
     *
     * @param result 回调参数
     * @param code   回调标记位
     */
    @Override
    public void onSuccess(String result, int code) {
        switch (code) {
            case HttpConstants.SEARCH_NEWS:
                break;
            default:
                break;
        }
    }

    @Override
    public void onStart(int code) {

    }

    @Override
    public void onFailure(int code) {

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
