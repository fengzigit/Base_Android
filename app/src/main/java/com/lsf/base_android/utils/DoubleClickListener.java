package com.lsf.base_android.utils;

import android.view.View;

import java.util.Calendar;

/**
 * 屏蔽重复点击
 */
public abstract class DoubleClickListener implements View.OnClickListener {

    public static final int MIN_CLICK_DELAY_TIME = 800;
    private long lastClickTime = 0;

    @Override
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime;
            onNoDoubleClick(v);
        }
    }

    public abstract void onNoDoubleClick(View v);
}
