package com.lsf.base_android.utils;

import android.app.Application;

/**
 * Created by lsf on 2016/1/25.
 */
public class MyApplication extends Application {
    public static LogUtil mLog;
    public static ToastUtil mToast;
    public OkHttpUtil mOkHttpUtil;

    @Override
    public void onCreate() {
        super.onCreate();
        //日志控制
        mLog = LogUtil.LSF_Log();

        //Toast控制
        mToast = new ToastUtil(this);

        //网络请求
        mOkHttpUtil = new OkHttpUtil();
    }
}
