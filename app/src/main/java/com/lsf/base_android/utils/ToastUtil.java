package com.lsf.base_android.utils;


import android.content.Context;
import android.widget.Toast;

/**
 * Toast工具类
 *
 * @author lsf
 * @time 2016/1/21 15:30
 */
public final class ToastUtil {
    private Toast mToast;
    private Context context;

    public ToastUtil(Context context) {
        // TODO Auto-generated constructor stub
        this.context = context;
    }

    public void showToast(String text) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.show();
    }

    public void showToastLong(String text) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.show();
    }

    public void cancelToast() {
        if (mToast != null) {
            mToast.cancel();
        }
    }
}
